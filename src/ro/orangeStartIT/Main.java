package ro.orangeStartIT;

import java.util.HashMap;
import java.util.Map;

/*Build an application where you will demonstrate the use of interface Map with HashMap.
        • Create a new empty map just as we did on sets
        • Add some employees types(bartenders, cashiers etc) using put() method
        • Print the total number of employees using size()
        • Iterate over all employees, using the keySet method and print the total type of that
        employee
        • An keySet Returns a set that contains all keys of the map
        • Clear all the values using clear() method
        • The final step, get the size of the employees(this should be 0, because you cleared all the
        values) and print it*/
public class Main {

    public static void main(String[] args) {
	// write your code here
        Map<String, Integer> EmployeeType = new HashMap<>();

        //Add some employees types(bartenders, cashiers etc) using put() method
        EmployeeType.put("Bartenders",5);
        EmployeeType.put("Receptionists",3);
        EmployeeType.put("Cashiers",10);
        EmployeeType.put("Porters",8);

        //Print the total number of employees using size()
        System.out.println("Total type of hotel employees: " + EmployeeType.size());

        //An keySet Returns a set that contains all keys of the map
        //Iterate over all employees, using the keySet method and print the total type of that employee
        for(String key: EmployeeType.keySet()){
            Integer value = EmployeeType.get(key);
            System.out.println(key + " - " + value);
        }

        System.out.println("Found total " + EmployeeType.get("Cashiers") +" Cashiers employees!");


        //Clear all the values using clear() method
        EmployeeType.clear();


        //The final step, get the size of the employees(this should be 0, because you cleared all the
        //values) and print it
        System.out.println("After clear operation size is now: " + EmployeeType.size());


    }
}
